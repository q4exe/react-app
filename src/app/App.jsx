import React from 'react';
import '../components/styles/main.scss';
import AppHeader from './app-header';
import AppAbout from './app-about';
import Diginityplus from './diginityplus';
import Scrinshots from './scrinshots';
import Reviews from './reviews';
import BuyNow from './buy-now';
import Contact from './contact';

export default function App() {
  return (
    <div className="container">
      <AppHeader />
      <AppAbout />
      <Diginityplus />
      <Scrinshots />
      <Reviews />
      <BuyNow />
      <Contact />
    </div>
  );
}
