import React from 'react';

export default function Diginityplus() {
  return (
    <div className="diginityplus-container">
      <div className="diginityplus_title text-center pt-5">Dignity and pluses product</div>
      <div className="d-flex flex-wrap px-4">
        <div className="diginityplus-block d-flex row pl-5 pt-4">
          <i className="fas fa-plus-square" />
          <div className="diginityplus-text pl-2">
            Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, <br />
            impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione <br />
            perferendis quas, maxime, quaerat porro totam, dolore minus inventore.
          </div>
        </div>
        <div className="diginityplus-block d-flex row pl-5 pt-4">
          <i className="fas fa-plus-square" />
          <div className="diginityplus-text pl-2">
            Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, <br />
            impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione <br />
            perferendis quas, maxime, quaerat porro totam, dolore minus inventore.
          </div>
        </div>
        <div className="diginityplus-block d-flex row pl-5 pt-4">
          <i className="fas fa-plus-square" />
          <div className="diginityplus-text pl-2">
            Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, <br />
            impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione <br />
            perferendis quas, maxime, quaerat porro totam, dolore minus inventore.
          </div>
        </div>
        <div className="diginityplus-block d-flex row pl-5 pt-4">
          <i className="fas fa-plus-square" />
          <div className="diginityplus-text pl-2">
            Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, <br />
            impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione <br />
            perferendis quas, maxime, quaerat porro totam, dolore minus inventore.
          </div>
        </div>
        <div className="diginityplus-block d-flex row pl-5 pt-4">
          <i className="fas fa-plus-square" />
          <div className="diginityplus-text pl-2">
            Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, <br />
            impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione <br />
            perferendis quas, maxime, quaerat porro totam, dolore minus inventore.
          </div>
        </div>
        <div className="diginityplus-block d-flex row pl-5 pt-4">
          <i className="fas fa-plus-square" />
          <div className="diginityplus-text pl-2">
            Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, <br />
            impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione <br />
            perferendis quas, maxime, quaerat porro totam, dolore minus inventore.
          </div>
        </div>
      </div>
    </div>
  );
}
