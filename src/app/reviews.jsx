import React from 'react';

export default function Reviews() {
  return (
    <div className="reviews-container">
      <div className="reviews_title text-center pt-5">Reviews</div>
      <div className="d-flex flex-wrap justify-content-around py-4 px-5">
        <div className="reviews-block d-flex align-items-start">
          <div className="scrinshots_block-avatar rounded-circle" />
          <div className="message-body d-flex pl-3">
            ,
            <div className="message-text p-3">
              Porro officia cumque sint deleniti nemo facere rem vitae odit inventore <br />
              cum odio, iste quia doloribus autem aperiam nulla ea neque reprehen- <br />
              derit. Libero doloribus, possimus officiis sapiente necessitatibus <br />
              commodi consectetur?
              <div className="message-author pt-2">Lourens S.</div>
            </div>
          </div>
        </div>
        <div className="reviews-block d-flex align-items-start">
          <div className="scrinshots_block-avatar rounded-circle" />
          <div className="message-body d-flex pl-3">
            <p className="left mt-4" />
            <div className="message-text p-3">
              Porro officia cumque sint deleniti nemo facere rem vitae odit inventore <br />
              cum odio, iste quia doloribus autem aperiam nulla ea neque reprehen- <br />
              derit. Libero doloribus, possimus officiis sapiente necessitatibus <br />
              commodi consectetur?
              <div className="message-author pt-2">Lourens S.</div>
            </div>
          </div>
        </div>
        <div className="reviews-block d-flex align-items-start pt-4">
          <div className="scrinshots_block-avatar rounded-circle" />
          <div className="message-body d-flex pl-3">
            <p className="left mt-4" />
            <div className="message-text p-3">
              Porro officia cumque sint deleniti nemo facere rem vitae odit inventore <br />
              cum odio, iste quia doloribus autem aperiam nulla ea neque reprehen- <br />
              derit. Libero doloribus, possimus officiis sapiente necessitatibus <br />
              commodi consectetur?
              <div className="message-author pt-2">Lourens S.</div>
            </div>
          </div>
        </div>
        <div className="reviews-block d-flex align-items-start pt-4">
          <div className="scrinshots_block-avatar rounded-circle" />
          <div className="message-body d-flex pl-3">
            <p className="left mt-4" />
            <div className="message-text p-3">
              Porro officia cumque sint deleniti nemo facere rem vitae odit inventore <br />
              cum odio, iste quia doloribus autem aperiam nulla ea neque reprehen- <br />
              derit. Libero doloribus, possimus officiis sapiente necessitatibus <br />
              commodi consectetur?
              <div className="message-author pt-2">Lourens S.</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
