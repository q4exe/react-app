import React from 'react';

export default function Scrinshots() {
  return (
    <div className="scrinshots-container">
      <div className="scrinshots_title text-center pt-5">Scrinshots</div>
      <div className="d-flex flex-wrap justify-content-around p-5">
        <div className="scrinshots_block d-flex row align-items-start">
          <div className="scrinshots_block-image" />
          <div className="pl-3">
            <div className="scrinshots_block-title">The description for the image</div>
            <div className="scrinshots_block-text pt-2">
              Pariatur iure ab sunt nesciunt, quibusdam odio iste <br />
              cumque itaque, ipsa vel exercitationem ullam quos <br />
              aut nostrum cupiditate fuga quaerat quam animi <br />
              dolores. Sequi itaque, unde perferendis nemo <br />
              debitis dolor.
            </div>
          </div>
        </div>
        <div className="scrinshots_block d-flex row align-items-start">
          <div className="scrinshots_block-image" />
          <div className="pl-3">
            <div className="scrinshots_block-title">The description for the image</div>
            <div className="scrinshots_block-text pt-2">
              Pariatur iure ab sunt nesciunt, quibusdam odio iste <br />
              cumque itaque, ipsa vel exercitationem ullam quos <br />
              aut nostrum cupiditate fuga quaerat quam animi <br />
              dolores. Sequi itaque, unde perferendis nemo <br />
              debitis dolor.
            </div>
          </div>
        </div>
        <div className="scrinshots_block d-flex row align-items-start pt-4">
          <div className="scrinshots_block-image" />
          <div className="pl-3">
            <div className="scrinshots_block-title">The description for the image</div>
            <div className="scrinshots_block-text pt-2">
              Lorem ipsum dolor sit amet, consectetur adipisic- <br />
              ing elit. Nobis facilis fuga, illo at. Natus eos, <br />
              eligendi illum rerum omnis porro ex, magni, expli- <br />
              cabo veniam incidunt in quam sapiente ut ipsum.
            </div>
          </div>
        </div>
        <div className="scrinshots_block d-flex row align-items-start pt-4">
          <div className="scrinshots_block-image" />
          <div className="pl-3">
            <div className="scrinshots_block-title">The description for the image</div>
            <div className="scrinshots_block-text pt-2">
              Lorem ipsum dolor sit amet, consectetur adipisic- <br />
              ing elit. Nobis facilis fuga, illo at. Natus eos, <br />
              eligendi illum rerum omnis porro ex, magni, expli- <br />
              cabo veniam incidunt in quam sapiente ut ipsum.
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
