import React from 'react';

export default function BuyNow() {
  return (
    <div className="buynow-container">
      <div className="buynow_title text-center pt-5">Buy it now</div>
      <div className="d-flex row justify-content-around align-items-end p-5">
        <div className="standart_block col-3 p-0">
          <div className="standart_block-title text-center pt-3">Standart</div>
          <div className="standart_block-coast text-center py-3 mt-3">$100</div>
          <div className="standart_block-text p-3">
            1. Porro officia cumque sint deleniti; <br />
            2. Тemo facere rem vitae odit; <br />
            3. Cum odio, iste quia doloribus autem; <br />
            4. Aperiam nulla ea neque.
          </div>
          <input className="py-3 ml-3 mb-3" type="button" name="standart-buy" value="BUY" />
        </div>
        <div className="standart_block col-3 p-0">
          <div className="standart_block-title text-center pt-3">Premium</div>
          <div className="standart_block-coast text-center py-3 mt-3">$150</div>
          <div className="standart_block-text p-3">
            1. Porro officia cumque sint deleniti; <br />
            2. Тemo facere rem vitae odit; <br />
            3. Cum odio, iste quia doloribus autem; <br />
            4. Aperiam nulla ea neque. <br />
            5. Porro officia cumque sint deleniti; <br />
            6. Тemo facere rem vitae odit; <br />
            7. Cum odio, iste quia doloribus autem; <br />
            8. Aperiam nulla ea neque.
          </div>
          <input className="py-3 ml-3 mb-3" type="button" name="standart-buy" value="BUY" />
        </div>
        <div className="standart_block col-3 p-0">
          <div className="standart_block-title text-center pt-3">Lux</div>
          <div className="standart_block-coast text-center py-3 mt-3">$200</div>
          <div className="standart_block-text p-3">
            1. Porro officia cumque sint deleniti; <br />
            2. Тemo facere rem vitae odit; <br />
            3. Cum odio, iste quia doloribus autem; <br />
            4. Aperiam nulla ea neque. <br />
            5. Porro officia cumque sint deleniti; <br />
            6. Тemo facere rem vitae odit; <br />
            7. Cum odio, iste quia doloribus autem; <br />
            8. Aperiam nulla ea neque. <br />
            9. Porro officia cumque sint deleniti; <br />
            10. Тemo facere rem vitae odit; <br />
            11. Cum odio, iste quia doloribus autem; <br />
            12. Aperiam nulla ea neque.
          </div>
          <input className="py-3 ml-3 mb-3" type="button" name="standart-buy" value="BUY" />
        </div>
      </div>
    </div>
  );
}
