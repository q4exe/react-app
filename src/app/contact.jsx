import React from 'react';

export default function Contact() {
  return (
    <div className="contact-container mt-4">
      <div className="contact_title text-center pt-5">Contacts</div>
      <div className="d-flex row justify-content-center p-4">
        <div className="contact-form col-5">
          <input className="form-control" type="text" placeholder="Your name:" />
          <input className="form-control mt-3" type="email" placeholder="Your email:" />
          <div className="form-group pt-3">
            <textarea
              className="form-control"
              id="exampleFormControlTextarea1"
              rows="6"
              placeholder="Your messange:"
            />
          </div>
          <button
            type="submit"
            className="btn px-5"
            style={{ backgroundColor: 'white', fontWeight: 'bold' }}
          >
            SEND
          </button>
        </div>
        <div className="person-contact text-white d-flex flex-column align-item-center pl-5">
          <div>
            <i className="fab fa-skype pr-3" style={{ fontSize: '30px' }} /> here_your_login_skype
          </div>
          <div>
            <i className="fas fa-envelope pt-3 pr-3" style={{ fontSize: '30px' }} />{' '}
            psdhtmlcss@mail.ru
          </div>
          <div>
            <i className="fas fa-phone-square-alt pt-3 pr-3" style={{ fontSize: '30px' }} /> 80 00
            4568 55 55
          </div>
        </div>
      </div>
    </div>
  );
}
