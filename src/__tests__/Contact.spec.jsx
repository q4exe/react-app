import React from 'react';
import { create } from 'react-test-renderer';
import Contact from '../app/contact';

describe('Contact component', () => {
  test('Matches the snapshot', () => {
    const contact = create(<Contact />);
    expect(contact.toJSON()).toMatchSnapshot();
  });
});
