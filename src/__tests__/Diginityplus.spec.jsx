import React from 'react';
import { create } from 'react-test-renderer';
import Diginityplus from '../app/diginityplus';

describe('Diginityplus component', () => {
  test('Matches the snapshot', () => {
    const diginityplus = create(<Diginityplus />);
    expect(diginityplus.toJSON()).toMatchSnapshot();
  });
});
