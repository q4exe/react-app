import React from 'react';
import { create } from 'react-test-renderer';
import BuyNow from '../app/buy-now';

describe('BuyNow component', () => {
  test('Matches the snapshot', () => {
    const buyNow = create(<BuyNow />);
    expect(buyNow.toJSON()).toMatchSnapshot();
  });
});
