import React from 'react';
import { create } from 'react-test-renderer';
import Scrinshots from '../app/scrinshots';

describe('Scrinshots component', () => {
  test('Matches the snapshot', () => {
    const scrinshots = create(<Scrinshots />);
    expect(scrinshots.toJSON()).toMatchSnapshot();
  });
});
