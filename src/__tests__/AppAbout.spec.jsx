import React from 'react';
import { create } from 'react-test-renderer';
import AppAbout from '../app/app-about';

describe('AppAbout component', () => {
  test('Matches the snapshot', () => {
    const appAbout = create(<AppAbout />);
    expect(appAbout.toJSON()).toMatchSnapshot();
  });
});
