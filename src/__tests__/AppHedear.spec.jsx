import React from 'react';
import { create } from 'react-test-renderer';
import AppHeader from '../app/app-header';

describe('AppHeader component', () => {
  test('Matches the snapshot', () => {
    const appHeader = create(<AppHeader />);
    expect(appHeader.toJSON()).toMatchSnapshot();
  });
});
